#include <string.h>
#include <stdint.h>
#include "sha2.h"

#define WORD_SIZE 32
typedef struct _node {
  uint8_t val[WORD_SIZE];
  uint32_t index;
} node;

void fill_depth_robust(uint8_t n, uint8_t *mem_start, uint8_t *challenge, uint8_t initial_cid);
