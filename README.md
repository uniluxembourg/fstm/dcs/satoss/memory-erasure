# Proof of Concept for `Software-Based Memory Erasure with relaxed isolation requirements`

We provide two implementations of the graph-based `PoSE` protocol, one in portable `C` (amenable for `IoT` devices), and one in `python`. The hash function was instantiated with SHA256.

## Note

This repository is a copy of the original (https://gitlab.uni.lu/regil/memory-erasure)