#! /usr/bin/env python3

from hashlib import sha256
WORD_SIZE = 32

def h(a):
	return sha256(a).digest()

def int2bytes(a):
	return a.to_bytes(4, 'little')

cid = None
preffix_challenge = None

def update_label_noid(pred0, pred1):
	global cid
	res = h(preffix_challenge + int2bytes(cid) + pred0 + pred1)
	cid += 1
	return res


def update_label_id(mem, p, p1, p2):
	l1 = mem[p1] if p1 is not None else b''
	l2 = mem[p2] if p2 is not None else b''
	mem[p] = update_label_noid(l1, l2)

def update_label_cross_id(mem, p1, p2):
	tmp = update_label_noid(mem[p1], mem[p2])
	mem[p2] = update_label_noid(mem[p1], mem[p2])
	mem[p1] = tmp


def apply_butterfly(mem, offset, n):
	n2 = 2 ** n

	def update_layer(jump):
		j = 0
		while j < n2:
			for k in range(j, j + jump):
				id1, id2 = k, k + jump
				update_label_cross_id(mem, id1 + offset, id2 + offset)
			j += 2 * jump

	for i in range(n):
		update_layer(2 ** (n - 1 - i))
	
	for j in range(n2):
		update_label_id(mem, j + offset, j + offset, None)
	
	for i in range(n):
		update_layer(2 ** i)

def fill_right(mem, offset, n):
	if n == 0:
		update_label_id(mem, offset, offset, None)
	else:

		for j in range(1, 2**n):
			update_label_id(mem, offset + j, offset + j, None)

		jj = 1
		for j in range(n):
			apply_butterfly(mem, offset + jj, j)
			jj += 2**j
		
		fill_full(mem , offset, n - 1, False)
		
		n21 = 2 ** (n - 1)
		for j in range(n21):
			update_label_id(mem, offset + n21 + j, offset + j, offset + n21 + j)
		
		apply_butterfly(mem, offset + n21, n - 1)
		
		fill_right(mem, offset + n21, n - 1)
		


def fill_full(mem, offset, n, first):
	if n == 0:
		if first:
			update_label_id(mem, offset, None, None)
		else:
			update_label_id(mem, offset, offset, None)
	else:
		fill_full(mem, offset, n - 1, first)
		
		n21 = 2 ** (n - 1)
		for j in range(n21):
			if first:
				update_label_id(mem,  offset + n21 + j, offset + j, None)
			else:
				update_label_id(mem,  offset + n21 + j, offset + j, offset + n21 + j)
		
		apply_butterfly(mem, offset + 2 ** (n - 1), n - 1)
		
		fill_right(mem, offset + 2 ** (n - 1), n - 1)

def fill(mem, n, challenge, initial_cid = False):
	global cid, preffix_challenge
	
	if initial_cid:
		cid = 0
	
	offset = 0
	preffix_challenge = challenge
	
	fill_full(mem, offset, n, True)
	
	for j in range(2 ** n):
		update_label_id(mem, offset + j, offset + j, None)

	apply_butterfly(mem, offset, n)

	fill_right(mem, offset, n)
	