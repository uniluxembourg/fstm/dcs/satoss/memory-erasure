#include "pose.h"


uint32_t cid;
uint8_t tmp[WORD_SIZE], input[3 * WORD_SIZE + 4];

void fill_depth_robust_full(uint8_t n, uint8_t *mem_start, uint8_t first);
void fill_depth_robust_right(uint8_t n, uint8_t *mem_start);



void h(uint8_t* output, uint8_t* input, uint8_t len){
  sha256_h(output, input, len);
}


void update_label_noid(uint8_t* addr, uint8_t* pred0, uint8_t* pred1){
  uint8_t len;


  
  memcpy(input + WORD_SIZE, &cid, 4);
  len = WORD_SIZE + 4;
  if(pred0 != NULL){
    memcpy(input + WORD_SIZE + 4, pred0, WORD_SIZE);
    len += WORD_SIZE;
  }
  if(pred1 != NULL){
    memcpy(input + 2 * WORD_SIZE + 4, pred1, WORD_SIZE);
    len += WORD_SIZE;
  }

  cid++;

  h(addr, input, len);
}


void update_layer(uint32_t jump, uint32_t n2, uint8_t *mem_start){
  int32_t j, k;

  j = 0;
  while(j < n2){
      for(k = j; k < j + jump; k++){
        update_label_noid(tmp, mem_start + k * WORD_SIZE, mem_start + (k + jump) * WORD_SIZE);
        
        update_label_noid(mem_start + (k + jump) * WORD_SIZE, mem_start + k * WORD_SIZE, mem_start + (k + jump) * WORD_SIZE);

        memcpy(mem_start + k * WORD_SIZE, tmp, WORD_SIZE);

      }
      j += 2 * jump;
  }
}


void apply_butterfly(uint8_t n, uint8_t *mem_start){
  int32_t i;
  uint32_t n2;

  n2 =  (uint32_t)1 << n;
  for(i = 0; i < n; i++){
      update_layer((uint32_t)1 << (n - 1 - i), n2, mem_start);
  }

  for(i = 0; i < n2; i++){
    update_label_noid(mem_start + i * WORD_SIZE, mem_start + i * WORD_SIZE, NULL);
  }

  for(i = 0; i < n; i++){
      update_layer((uint32_t)1 << i, n2, mem_start);
  }

}


void fill_depth_robust_right(uint8_t n, uint8_t *mem_start){
  uint32_t j, jj, n21;
  if(n == 0){
    update_label_noid(mem_start, mem_start, NULL);
  }
  else{
    
    
    for(j = 1; j < (1<<n); j++){
      update_label_noid(mem_start + j * WORD_SIZE, mem_start + j * WORD_SIZE, NULL);
    }

    jj = 1;
    for(j = 0; j < n; j++){
        apply_butterfly(j, mem_start + jj * WORD_SIZE);
        jj += (1 << j);
    }

    fill_depth_robust_full(n - 1, mem_start, 0);
    
    n21 = (uint32_t)1 << (n - 1);
    for(j = 0; j < n21; j++){
      update_label_noid(mem_start + (n21 + j) * WORD_SIZE, mem_start + j * WORD_SIZE, mem_start + (n21 + j) * WORD_SIZE);
    }
    
    apply_butterfly(n - 1, mem_start + n21 * WORD_SIZE);

    fill_depth_robust_right(n - 1, mem_start + n21 * WORD_SIZE);
  }
}

void fill_depth_robust_full(uint8_t n, uint8_t *mem_start, uint8_t first){
  uint32_t j, n21;

  if(n == 0){
      if(first){
          update_label_noid(mem_start, NULL, NULL);
      }
      else{
          update_label_noid(mem_start, mem_start, NULL);
      }
  }
  else{
    fill_depth_robust_full(n - 1, mem_start, first);
    
    n21 = (uint32_t)1 << (n - 1);
    for(j = 0; j < n21; j++){
      if(first){
        update_label_noid(mem_start + (n21 + j) * WORD_SIZE, mem_start + j * WORD_SIZE, NULL);
      }
      else{
        update_label_noid(mem_start + (n21 + j) * WORD_SIZE, mem_start + j * WORD_SIZE, mem_start + (n21 + j) * WORD_SIZE);
      }
    }
    
    apply_butterfly(n - 1, mem_start + n21 * WORD_SIZE);

    fill_depth_robust_right(n - 1, mem_start + n21 * WORD_SIZE);
    
    
  }
}


void fill_depth_robust(uint8_t n, uint8_t *mem_start, uint8_t *challenge, uint8_t initial_cid){
  uint32_t n2, j;

  n2 = (uint32_t) 1 << n;
  memcpy(input, challenge, WORD_SIZE);

  if(initial_cid > 0) cid = 0;

  fill_depth_robust_full(n, mem_start, 1);

  for(j = 0; j < n2; j++){
    update_label_noid(mem_start + j * WORD_SIZE, mem_start + j * WORD_SIZE, NULL);
  }

  apply_butterfly(n, mem_start);

  fill_depth_robust_right(n, mem_start);

}










