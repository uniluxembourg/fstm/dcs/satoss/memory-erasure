SOURCES_TEST    = test.c pose.c  sha2.c
OBJECTS_TEST    = test.o pose.o  sha2.o
CFLAGS_TEST     = -Os -m32
CC_TEST         = gcc

.PHONY: clean

clean:
	rm -f tests *.o


compile_test: ${SOURCES_TEST}
	${CC_TEST} ${CFLAGS_TEST} ${SOURCES_TEST} -c 
	${CC_TEST} ${CFLAGS_TEST} -o tests ${OBJECTS_TEST}
	
test: compile_test
	./tests