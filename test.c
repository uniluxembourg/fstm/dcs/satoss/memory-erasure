#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <stdlib.h>
#include "pose.h"

#define MEM_SIZE_LOG 10

uint8_t mem[((uint32_t)1 << MEM_SIZE_LOG) * WORD_SIZE];

int main(int argc, char *argv[]){
    uint8_t challenge[WORD_SIZE];
    int8_t i;
    clock_t begin, end;
    int32_t t;
    srand(time(NULL));
    for(i = 0; i < WORD_SIZE; i++)challenge[i] = rand() % 256;
    begin = clock();
    fill_depth_robust(MEM_SIZE_LOG, mem, challenge, 1);
    end = clock();
    printf("PoSE-DB-graph compute time for %d bytes: %fs\n", ((uint32_t)1 << MEM_SIZE_LOG) * WORD_SIZE, (double)(end - begin) / CLOCKS_PER_SEC);
    return 0;
}
